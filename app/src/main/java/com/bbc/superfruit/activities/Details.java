package com.bbc.superfruit.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bbc.superfruit.R;
import com.bbc.superfruit.fruit.Fruit;
import com.bbc.superfruit.utils.Event;

/**
 * @author - Sohail Saddique
 * This is the Fruit Details class. This class displays, correctly, the passed information from
 * the previous activity (which fruit the user clicked and the associated details). It then sets up
 * the menu clicks and additionally, the text fields.
 */
public class Details extends AppCompatActivity {

    ProgressBar progressBar;
    private Fruit fruit;
    private Event event;
    private static final String TAG = "Details";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        event = Event.initialise(this);
        setupClicks();
        // Get the clicked fruit from previous activity
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            fruit = (Fruit) extras.get("clickedFruit");
            Log.i(TAG, extras.toString());
        }
        initialise();
        Log.i(TAG, "Intialised activity successfully");
    }

    /**
     * Override the back button's default transition animation to my own
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
        finish();
    }

    /**
     * Override this method to capture display time of activity
     */
    @Override
    protected void onResume() {
        super.onResume();
        event.endDisplay();
    }

    /**
     * Set the text fields and images onto the corresponding Image/Text Views
     */
    private void initialise() {
        progressBar = findViewById(R.id.progressBarH);
        progressBar.setVisibility(View.INVISIBLE);

        setTitle(fruit.getType());
        setFruit();
        if (fruit.getType().equals("Apple")) {
            ImageView imageview = findViewById(R.id.img_fruit);
            imageview.setImageResource(R.drawable.apple);
            TextView textView = findViewById(R.id.txt_fruitdetail);
            textView.setText(R.string.appleDetails);
        }
        if (fruit.getType().equals("Banana")) {
            ImageView imageview = findViewById(R.id.img_fruit);
            imageview.setImageResource(R.drawable.banana);
            TextView textView = findViewById(R.id.txt_fruitdetail);
            textView.setText(R.string.bananaDetails);
        }
        if (fruit.getType().equals("Blueberry")) {
            ImageView imageview = findViewById(R.id.img_fruit);
            imageview.setImageResource(R.drawable.blueberry);
            TextView textView = findViewById(R.id.txt_fruitdetail);
            textView.setText(R.string.blueberryDetails);
        }
        if (fruit.getType().equals("Orange")) {
            ImageView imageview = findViewById(R.id.img_fruit);
            imageview.setImageResource(R.drawable.orange);
            TextView textView = findViewById(R.id.txt_fruitdetail);
            textView.setText(R.string.orangeDetails);
        }
        if (fruit.getType().equals("Pear")) {
            ImageView imageview = findViewById(R.id.img_fruit);
            imageview.setImageResource(R.drawable.pear);
            TextView textView = findViewById(R.id.txt_fruitdetail);
            textView.setText(R.string.pearDetails);
        }
        if (fruit.getType().equals("Strawberry")) {
            ImageView imageview = findViewById(R.id.img_fruit);
            imageview.setImageResource(R.drawable.strawberry);
            TextView textView = findViewById(R.id.txt_fruitdetail);
            textView.setText(R.string.strawberryDetails);
        }
        if (fruit.getType().equals("Kumquat")) {
            ImageView imageview = findViewById(R.id.img_fruit);
            imageview.setImageResource(R.drawable.kumquat);
            TextView textView = findViewById(R.id.txt_fruitdetail);
            textView.setText(R.string.kumquatDetails);
        }
        if (fruit.getType().equals("Pitaya")) {
            ImageView imageview = findViewById(R.id.img_fruit);
            imageview.setImageResource(R.drawable.pitaya);
            TextView textView = findViewById(R.id.txt_fruitdetail);
            textView.setText(R.string.pitayaDetails);
        }
        if (fruit.getType().equals("Kiwi")) {
            ImageView imageview = findViewById(R.id.img_fruit);
            imageview.setImageResource(R.drawable.kiwi);
            TextView textView = findViewById(R.id.txt_fruitdetail);
            textView.setText(R.string.kiwiDetails);
        }
    }

    /**
     * Get the clicked fruit from previous activity and set the details onto the
     * corresponding text fields
     */
    private void setFruit() {
        TextView fruit_header = findViewById(R.id.chosen_fruit);
        fruit_header.setText(fruit.getType());
        TextView tvPrice = findViewById(R.id.txt_price);
        TextView tvWeight = findViewById(R.id.txt_weight);
        String price = ("£" + (String.valueOf(fruit.getPrice())));
        String weight = (String.valueOf(fruit.getWeight()) + " kg");
        tvPrice.setText(price);
        tvWeight.setText(weight);
    }

    /**
     * Handle click events on the toolbar
     */
    private void setupClicks() {
        final Toolbar toolbar = findViewById(R.id.home_toolbar);
        toolbar.inflateMenu(R.menu.home_menu);
        ActionMenuItemView refresh = findViewById(R.id.menu_refresh);
        refresh.setVisibility(View.INVISIBLE);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_about:
                        Toast.makeText(getApplicationContext(), "App created by Sohail Saddique",
                                Toast.LENGTH_LONG).show();
                        return true;
                }
                return false;
            }
        });
    }
}