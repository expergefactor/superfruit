package com.bbc.superfruit.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bbc.superfruit.R;
import com.bbc.superfruit.fruit.FruitAdapter;
import com.bbc.superfruit.fruit.FruitParser;
import com.bbc.superfruit.utils.Event;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author - Sohail Saddique
 * This is the Home Activity class. This will be the first screen the user sees.
 * This class handles the click listeners and also parses the JSON data using the provided URL.
 */
public class Home extends AppCompatActivity {

    private ProgressBar progressBar;
    private static final String TAG = "Home";
    private Event event;
    public String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        event = Event.initialise(this);
        initialise();
    }

    /**
     * Initiate and setup elements - handle click events and JSON parsing
     */
    private void initialise() {
        progressBar = findViewById(R.id.progressBarH);
        setupClicks();
        setupJSON();
        progressBar.setVisibility(View.INVISIBLE);
        Log.i(TAG, "Initialised successfully");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
        Log.i(TAG, "Activity shut down");
    }

    @Override
    protected void onResume() {
        super.onResume();
        event.endDisplay();
    }

    /**
     * This method handles click events for the toolbar, including the refresh button
     */
    private void setupClicks() {
        final Toolbar toolbar = findViewById(R.id.home_toolbar);
        toolbar.inflateMenu(R.menu.home_menu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_about:
                        Toast.makeText(getApplicationContext(), "App created by Sohail Saddique",
                                Toast.LENGTH_LONG).show();
                        return true;
                    case R.id.menu_refresh:
                        setupJSON();
                        Toast.makeText(getApplicationContext(), "Refreshing...",
                                Toast.LENGTH_SHORT).show();
                        return true;
                }
                return false;
            }
        });
    }

    /**
     * JSON Parsing and ProgressBar handling
     */
    public void setupJSON() {
        progressBar = findViewById(R.id.progressBarH);
        progressBar.setVisibility(View.VISIBLE);
        url = "https://raw.githubusercontent.com/fmtvp/recruit-test-data/master/data.json";
        StringRequest request = new StringRequest(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String string) {
                parseJsonData(string);
                Log.i(TAG, ("Connected to: "+ url));
                event.endLoad();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(getApplicationContext(), "Unable to connect to database",
                        Toast.LENGTH_LONG).show();
                progressBar.setVisibility(View.INVISIBLE);
                Log.e(TAG, "No network connection or server offline");
            }
        });
        RequestQueue rQueue = Volley.newRequestQueue(Home.this);
        rQueue.getCache().clear();
        rQueue.add(request);
        event.startLoad();
    }

    /**
     *
     * @param jsonString - Parsing the data
     */
    private void parseJsonData(String jsonString) {
        try {
            // Find the listview where the fruits will be placed
            ListView fruitsListView = findViewById(R.id.fruitsList);
            // onResponse - receive the parsed URL JSON object
            JSONObject object = new JSONObject(jsonString);
            // Create new FruitParser
            FruitParser fruitParser = new FruitParser();
            // Parse the fruits from the passed object (parsed URL)
            fruitParser.parseFruits(object);
            // Create new ArrayAdapter and get the list of parsed fruits
            FruitAdapter fruitAdapter = new FruitAdapter(this, fruitParser.getListOfFruits());
            // Set the listview adapter to the FruitAdapter
            fruitsListView.setAdapter(fruitAdapter);
            // Setup click listener on listview
            fruitsListView.setOnItemClickListener(fruitAdapter);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, e.toString());
        }
        // Control progress bar refresh time
        Thread thread = new Thread(){
            @Override
            public void run() {
                try {
                    synchronized (this) {
                        wait(1600);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressBar.setVisibility(View.INVISIBLE);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    Log.e(TAG, e.toString());
                }
            }
        };
        thread.start();
    }
}
