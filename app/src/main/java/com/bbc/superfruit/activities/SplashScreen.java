package com.bbc.superfruit.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.bbc.superfruit.R;

/**
 * @author - Sohail Saddique
 * Splashscreen activity which shows the name of the app "Superfruit" and transitions to the
 * Home activity with a shared element animation.
 */
public class SplashScreen extends AppCompatActivity {

    Handler handler;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        animate();
    }

    private void animate() {
        imageView = findViewById(R.id.superfruit_splash);
        handler  = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashScreen.this, Home.class);
                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        SplashScreen.this,
                        imageView, "transition");
                startActivity(intent, options.toBundle());
            }
        },1600);
    }
}


