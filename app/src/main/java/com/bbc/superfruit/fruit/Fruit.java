package com.bbc.superfruit.fruit;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author - Sohail Saddique
 * This class setups the Fruit object. In accordance with the specification, a fruit object contains
 * a type, a price and a weight. Fruit implements Parcelable to allow data to be passed into intent
 * bundle (putExtra).
 */

public class Fruit implements Parcelable {
    private String mType;
    private double mPrice;
    private double mWeight;

    public Fruit(String type, double price, double weight){
        this.mType = type;
        this.mPrice = price/100;
        this.mWeight = weight/1000;
    }

    public String getType(){
        return mType;
    }

    public double getPrice(){
        return mPrice;
    }

    public double getWeight(){
        return mWeight;
    }

    /**
     *
     * @return - Returns the converted object into string format
     */
    @Override
    public String toString() {
        return "Fruit{" +
                "" + mType +
                "," + mPrice +
                "," + mWeight +
                '}';
    }


    public Fruit(Parcel input){
        this.mType = input.readString();
        this.mPrice = input.readDouble();
        this.mWeight = input.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mType);
        dest.writeDouble(mPrice);
        dest.writeDouble(mWeight);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Fruit createFromParcel(Parcel in)
        {
            return new Fruit(in);
        }

        public Fruit[] newArray(int size)
        {
            return new Fruit[size];
        }
    };
}