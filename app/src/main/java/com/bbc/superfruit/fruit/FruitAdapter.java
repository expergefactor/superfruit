package com.bbc.superfruit.fruit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.bbc.superfruit.R;
import com.bbc.superfruit.activities.Details;

import java.util.ArrayList;

/**
 * @author - Sohail Saddique
 * This class implements a custom ArrayAdapter which handles the click position, and therefore
 * carrying over this data and storing it for the next activity.
 */
public class FruitAdapter extends android.widget.ArrayAdapter<Fruit> implements AdapterView.OnItemClickListener {
    private final Context context;
    private final ArrayList<Fruit> clickedFruit;

    /**
     * @param context - Gets the context of the activity (in this case, Home)
     * @param clickedFruit - ArrayList to store the clicked fruit information
     */
    public FruitAdapter(Context context, ArrayList<Fruit> clickedFruit) {
        super(context, android.R.layout.simple_list_item_1, clickedFruit);
        this.context = context;
        this.clickedFruit = clickedFruit;
    }

    /**
     * @param convertView - Convert each row in the view to my custom defined layout
     * @return - Return the converted view
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.listview_row, parent, false);
        TextView textView = convertView.findViewById(R.id.tvFruitType);
        textView.setText(clickedFruit.get(position).getType());
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(context, Details.class);
        // Pass the information of the fruit according to which one was clicked
        intent.putExtra("clickedFruit", clickedFruit.get(position));
        context.startActivity(intent);
        ((Activity)context).overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

}
