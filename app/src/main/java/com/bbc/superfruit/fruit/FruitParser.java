package com.bbc.superfruit.fruit;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * @author - Sohail Saddique
 * This class receives the parsed url and stores it
 * in an arraylist to be used to populate the listview and also the Details activity.
 */
public class FruitParser {

    private static final String FRUIT = "fruit";
    private static final String TYPE = "type";
    private static final String PRICE = "price";
    private static final String WEIGHT = "weight";
    private ArrayList<Fruit> listOfFruits;

    public FruitParser() {
        listOfFruits = new ArrayList<>();
    }

    /**
     * @return - Returns the arraylist created in the previous method
     */
    public ArrayList<Fruit> getListOfFruits(){
        return listOfFruits;
    }

    /**
     * @param jsonObject - Gets the JSONObject from the provided url
     */
    public void parseFruits(JSONObject jsonObject){
        try  {
            if (jsonObject.has(FRUIT)) {
                JSONArray fruits = jsonObject.getJSONArray(FRUIT);
                for(int i = 0; i < fruits.length(); i++){
                    JSONObject fruitJsonObject = (JSONObject) fruits.get(i);
                    String type;
                    String typeCap = "";
                    String price = "";
                    String weight = "";

                    if(fruitJsonObject.has(TYPE)){
                        type = fruitJsonObject.getString(TYPE);
                        typeCap = type.substring(0, 1).toUpperCase() + type.substring(1);
                    }
                    if(fruitJsonObject.has(PRICE)){
                        price = fruitJsonObject.getString(PRICE);
                    }
                    if(fruitJsonObject.has(WEIGHT)){
                        weight = fruitJsonObject.getString(WEIGHT);
                    }
                    // Create new fruit
                    Fruit parsedFruit = new Fruit(typeCap, Double.parseDouble(price),Double.parseDouble(weight));
                    // And add the fruit(s) to the arraylist
                    listOfFruits.add(parsedFruit);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("FruitParser", e.toString());
        }
    }
}
