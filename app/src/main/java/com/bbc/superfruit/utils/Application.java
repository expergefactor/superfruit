package com.bbc.superfruit.utils;

public class Application extends android.app.Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ExceptionHandler.installHandler();
    }
}