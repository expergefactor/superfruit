package com.bbc.superfruit.utils;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

/**
 * @author - Sohail Saddique
 * This class implements the Analytic Requirements of the app. Event is a "singleton" class which
 * can have only one object (an instance of the class) at a time.
 */
public class Event {

    private static Event event;
    private static String statURL = "https://raw.githubusercontent.com/fmtvp/recruit-test-data/master/stats";
    private RequestQueue rQueue;
    private long loadStart;

    private Event() {}

    public static Event initialise(Context context) {
        if (event == null) {
            event = new Event();
            event.rQueue = Volley.newRequestQueue(context);
        }
        return event;
    }

    /**
     * @param url - Gets the url (constructed from the methods below) and issues the request
     */
    private void fireAndForget(String url) {
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                });
        rQueue.add(request);
    }

    public void startLoad() {
        loadStart = System.currentTimeMillis();
    }

    public void endLoad() {
        long difference = System.currentTimeMillis() - loadStart;
        String url = statURL + "?event=load&data=" + difference;
        fireAndForget(url);
        Log.i("Event_load", url);
    }

    public void endDisplay() {
        long timestamp = System.currentTimeMillis();
        String url = statURL + "?event=display&data=" + timestamp;
        fireAndForget(url);
        Log.i("Event_display", url);
    }
}