package com.bbc.superfruit.utils;

import android.app.Application;
import android.util.Log;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

public class ExceptionHandler extends Application implements Thread.UncaughtExceptionHandler {


    private Thread.UncaughtExceptionHandler defaultHandler;
    private static final String TAG = "ApplicationCrashHandler";

    /**
     * Installs a new exception handler.
     */
    public static void installHandler() {
        if (!(Thread.getDefaultUncaughtExceptionHandler() instanceof ExceptionHandler)) {
            Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler());
        }
    }

    private ExceptionHandler() {
        this.defaultHandler = Thread.getDefaultUncaughtExceptionHandler();
    }

    /**
     * Called when there is an uncaught exception elsewhere in the code.
     * @param t - the thread that caused the error
     * @param e - the exception that caused the error
     */
    @Override
    public void uncaughtException(Thread t, Throwable e) {
        // Place a breakpoint here to catch application crashes
        Log.wtf(TAG, String.format("Exception: %s\n%s", e.toString(), getStackTrace(e)));

        // Call the default handler
        defaultHandler.uncaughtException(t, e);
    }

    /**
     * Convert an exception into a printable stack trace.
     * @param e the exception to convert
     * @return the stack trace
     */
    private String getStackTrace(Throwable e) {
        final Writer sw = new StringWriter();
        final PrintWriter pw = new PrintWriter(sw);

        e.printStackTrace(pw);
        String stacktrace = sw.toString();
        pw.close();
        return stacktrace;
    }
}