package com.bbc.superfruit.activities;

import android.content.Intent;
import android.test.ActivityUnitTestCase;
import android.test.suitebuilder.annotation.MediumTest;

import org.junit.Assert;

/**
 * @author Sohail Saddique
 * Not much can be tested here while utilising JUnit and performing unit tests.
 * However, I have included two Espresso tests which can be found under the /androidTest/ directory
 */
public class UnitTest extends ActivityUnitTestCase<Home> {

    private Intent mIntent;

    public UnitTest() {
        super(Home.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mIntent = new Intent(Intent.ACTION_MAIN);
    }

    @MediumTest
    public void testMainActivity() {
        Home home = startActivity(mIntent, null, null);
        Assert.assertNotNull(home);
    }
}